<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
        <h1>EDITAR ALUMNO</h1>
        <form method="POST" action="{{ route('alumnos.update', [$alumno->id]) }}">
            @csrf
            @method('PUT') <!-- Utiliza PUT para la actualización -->
            <input type="text" id="dni" name="dni" value="{{ $alumno->dni }}">
            <input type="text" id="nombres" name="nombres" value="{{ $alumno->nombres }}">
            <input type="text" id="apellidos" name="apellidos" value="{{ $alumno->apellidos }}">
                
            <button type="submit" class="btn btn-primary">Actualizar</button>
        </form>   

</body>
</html>