<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Alumno;
use App\Models\Matricula;

use Illuminate\Support\Facades\DB;
use App\Utils\LogHelper;

class MatriculaController extends Controller
{
    public function testCreate(Request $request)
    {
        
        DB::beginTransaction();
        try{
            $alumno = new Alumno();
            $alumno->dni = "87451111";
            $alumno->nombres = "jorge";
            $alumno->apellidos = "cotrado";
            $alumno->save();
            $idAlumno=$alumno->id;

            $matricula = new Matricula();
            $matricula->anioAcad= "2028-I";
            $matricula->idAlumno = $idAlumno;
            $matricula->save();
            
            DB::commit();

            return "CREADO MATRICULA OK";
           
        }catch(\Exception $e){

            DB::rollback();
            LogHelper::logError($this,$e);
            $fechaHoraActual = date("Y-m-d H:i:s");
            return $fechaHoraActual." NO SE PUDO REALIZAR LA OPERACION";
        }        
        
    }//fin de la funcion
}//fin de la clase
