<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Alumno;

class AlumnoController extends Controller
{
    public function index()
    {
        $alumnos = Alumno::all();
        return view('alumnos.index', compact('alumnos'));
    }

    public function create(Request $request)
    {
        return view('alumnos.create');
    }

    public function store(Request $request)
    {
            $request->validate([
                'nombres' => 'required',
                'apellidos' => 'required',
                'dni' => 'required|digits:8|unique:alumnos,dni',
            ]);
         
        try{
            
            $alumno=Alumno::create($request->all());
            $idAlumno = $alumno->id;
            session(["idAlumno"=>$idAlumno]);

            return redirect()->route('alumnos.index');
        }
        catch(\Exception $e){
            
            $errorMessage = $e->getMessage(); 
            return redirect()->route('alumnos.create')->with('error', $errorMessage);
        }
    }
    public function edit($id)
    {   
        $alumno = Alumno::findOrFail($id);
        return view('alumnos.edit', compact('alumno'));
    }
    public function update(Request $request, $id)
    {
        
        $alumno = Alumno::findOrFail($id);
        
        $alumno->update([
            'nombres' => $request->nombres,
            'apellidos' => $request->apellidos,
            'dni' => $request->dni,
        ]);

        session()->forget("idAlumno");
        return redirect()->route('alumnos.index');
    }
    public function destroy($id)
    {
  
        $alumno = Alumno::findOrFail($id);
        $alumno->delete();

        //$alumnos = Alumno::all();
        //return view('alumnos.index', compact('alumnos'));
        return redirect()->route('alumnos.index');
    }
}
